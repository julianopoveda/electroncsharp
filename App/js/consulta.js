var edge = require('electron-edge');

document.onreadystatechange = function(){
    if(document.readyState == "complete"){
        var button = document.getElementById("consulta");

        button.onclick = function(){
            //debugger;
            var resultado; 
            consultaRegras(null, function(error, result){
                console.log(result);
                resultado = result;
            });
            var div = document.getElementById('resultado');
            for(var i=0; i<resultado.length; i++){
                var p = document.createElement("p");
                var br = document.createElement("br");
                var faixa = resultado[i].Faixa;
                p.textContent = "Faixa de aplicacao: " + faixa.Minimo + " - " + faixa.Maximo
                p.appendChild(br);
                p.textContent += " Codigo da Mensagem: " + resultado[i].CodigoIdentificacao;
                p.appendChild(br);
                p.textContent += " Validacao: " + resultado[i].Validacao;

                div.appendChild(p);
            }
        };
    }
}

var consultaRegras = edge.func({
    assemblyFile: __dirname.replace('Views','dlls') + '/Entidades.dll',
    typeName: 'Entidades.Comunicacao',
    methodName: 'ConsultarRegras'
})