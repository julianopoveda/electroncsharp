const electron = require('electron');
const {app, BrowserWindow} = electron;
const url = require('url');
const path = require('path');


var edge = require('electron-edge');

app.on('ready', function(){
    var mainWindow = new BrowserWindow({width: 1024, height: 768});
    
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'App/Views/index.html'),
        protocol: 'file:',
        slashes:true
    }));

    mainWindow.webContents.openDevTools();//Remover no deploy. Abre as ferramentas do desenvolvedor    
});